import React, { Component } from 'react';
import {Header, Footer, Content} from './sections';
import './style.scss';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Header/>
        <Content/>
        <Footer/>
      </div>
    );
  }
}

export default App;
