# File Structure
## Pre-requisite
* The Markdown is written in VSCode and is best to be previewed using Markdown Preview command 
keyboard shortcut: <kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>v</kbd>
* Understanding of [es6](https://www.tutorialspoint.com/es6/index.htm)'s [import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) and [export](https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export) features is essential.
* Understanding of [react components](https://reactjs.org/tutorial/tutorial.html#overview) will be useful.


## Introduction
The filestructure itself reflect the DOM structure modulated as different [react components](https://reactjs.org/tutorial/tutorial.html#overview) under different directory. Files are modulated based on directory with index.js. This index js file exports the modules that will be imported whenever a directory is imported. 

**For Example :**
*on [./App/index.js:2](./App/index.js#L2) we are importing different module from sections.*
```jsx
import {Header, Footer, Content} from './sections';
```
*sections is actually a directory with [index.js](./App/sections/index.js). The [index file](./App/sections/index.js) is what is exporting all these module.*

```jsx
export {Content} from './Content';
export {Header} from './Header';
export {Footer} from './Footer';
```
*The variables declare under curly braces (for example: ```{Header}```) are exported in index.js file located on the ```from``` directory ( ```from './Header'```).

i.e. under the [Header directory](./App/sections/Header), there is [index.js](./App/sections/Header/index.js). In index.js Header component is exported.*
```jsx
export class Header extends Component {
```

The style is also modulated alongside the DOM component. All the components declared under index.js are followed with style.scss file in same directory. These styles are imported using following line of code:

```jsx
import './style.scss';
```
The modulation of styles are useful for keeping the local component style classes neatly seperate from shared style classes.  While global styles nd other styles will be structured under [style directory](./style).

### How components are rendered
These components are rendered on [div#root](../public/index.html#L28) in [index.html](../public/index.html). The index file is located under [public folder](../public). [index.js](./index.js) on current folder ([src](./)) hooks base App component to div with id 'root' with following codes.

*on [index.js:7](./index.js#L7)*
```jsx
ReactDOM.render(<App />, document.getElementById('root'));
```

*on [public/index.html:28](../public/index.html#L28)*
```html
    <div id="root"></div>
```
### Component declaration structure
The [App directory](./App) is where the main react component is declared. This defines the structure of DOM element to be rendered under [div#root](../public/index.html#L28).


```jsx
class App extends Component {
  render() {
    return (
      <div className="app">
        <Header/>
        <Content/>
        <Footer/>
      </div>
    );
  }
}
``` 

The exported App component defines [render function](https://reactjs.org/docs/react-component.html#render) with [JSX](https://reactjs.org/docs/introducing-jsx.html) code (that resembles html) outlining the overall structure of our DOM.
